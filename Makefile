GOPATH ?= $(HOME)/go
PATH := $(GOPATH)/bin:$(PATH)
DESTDIR ?= /

.PHONY: all
all: bins

.PHONY: bins
bins:
	mkdir -p bin
	go build -o bin/installer cmd/installer/*.go

.PHONY: clean
clean:
	rm -rf bin/

.PHONY: deps
deps:
	go get ./...

.PHONY: fmt
fmt:
	find cmd/ internal/ -name '*.go' | xargs gofmt -w -s

.PHONY: vendor
vendor:
	GO111MODULE=on go mod vendor

.PHONY: test
test:
	@echo "TODO: we should have some tests"

.PHONY: install-bins
install-bins: bins
	install -m 0755 bin/installer $(DESTDIR)/$(bindir)

.PHONY: install-confs
install-confs:
	mkdir -p $(DESTDIR)/$(sysconfdir)
	install -m 0644 conf/installer.yaml $(DESTDIR)/$(sysconfdir)

.PHONY: install
install: install-bins install-confs

.PHONY: check
check: golint all test
	DESTDIR=/tmp make install
	go install `go list -f  "{{.ImportPath}}" "{{.TestGoFiles}}" ./...`

.PHONY: golint
golint:
	golangci-lint --verbose run --enable-all -Dgochecknoglobals -Dgochecknoinits -Dlll

