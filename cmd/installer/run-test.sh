#!/bin/bash
go build main.go || exit
sudo modprobe nbd
sudo qemu-img create testimg.qcow2 -f qcow2 30G
sudo qemu-nbd -c /dev/nbd0 -f qcow2 testimg.qcow2
sudo ./main -install-config ../../conf/partition.yaml || echo "Failed"
sudo parted /dev/nbd0 print
sudo pvscan
sudo vgscan
sudo lvscan
sudo echo -ne 'password' > /tmp/keyfile
sudo cryptsetup luksOpen --key-file /tmp/keyfile /dev/dom0/storage storage
sudo cryptsetup luksOpen --key-file /sys/class/dmi/id/product_uuid /dev/dom0/root root 
sudo mkdir -p /tmp/storage
sudo mount /dev/mapper/storage /tmp/storage
sudo mkdir -p /tmp/root
sudo mount /dev/mapper/root /tmp/root
sudo mkdir -p /tmp/ESP
sudo mount /dev/disk/by-partlabel/ESP /tmp/ESP
/bin/bash
sudo umount /dev/mapper/storage
sudo umount /dev/mapper/root
sudo umount /dev/disk/by-partlabel/ESP
sudo cryptsetup luksClose storage
sudo cryptsetup luksClose root
sudo vgremove -ff -y dom0
sleep 5
sudo qemu-nbd -d /dev/nbd0
sudo rm testimg.qcow2
sudo rm -rf /tmp/root /tmp/storage /tmp/ESP

