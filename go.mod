module gitlab.com/redfield/installer

require (
	github.com/andlabs/ui v0.0.0-20180902183112-867a9e5a498d // indirect
	github.com/pkg/errors v0.8.1
	gitlab.com/redfield/cryptctl v0.0.0-20190820190921-227d9ee95d14
	gopkg.in/yaml.v2 v2.2.2
)
