// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package installer

import (
	"fmt"
	"io/ioutil"

	"gitlab.com/redfield/cryptctl"
)

// DiskCrypt holds info for doing disk encryption in the installer
type DiskCrypt struct {
	Path       string `yaml:"path"`
	AuthMode   string `yaml:"auth_mode"`
	Key        string `yaml:"key"`
	KeyType    string `yaml:"key_type"`
	TargetName string `yaml:"target_name"`
}

func (d DiskCrypt) String() string {
	return fmt.Sprintf("Path: %v\nAuthMode: %v\nKey: %v\nKeyType: %v\nTargetName: %v", d.Path, d.AuthMode, d.Key, d.KeyType, d.TargetName)
}

// CreateDiskCrypt Create an encrypted disk volume.
func CreateDiskCrypt(dc DiskCrypt) error {
	var err error
	switch dc.KeyType {
	case "string":
		{
			err = cryptctl.LuksFormatVolume(dc.Path, []byte(dc.Key))
			if err != nil {
				return err
			}
		}
	case "path":
		{
			keyfile, err := ioutil.ReadFile(dc.Key)
			if err != nil {
				return err
			}

			err = cryptctl.LuksFormatVolume(dc.Path, keyfile)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

// CreateDiskCrypts Create a set of encrypted disk volumes.
func CreateDiskCrypts(dcs []DiskCrypt) error {
	for _, dc := range dcs {
		err := CreateDiskCrypt(dc)
		if err != nil {
			return err
		}
	}

	return nil
}

// OpenDiskCrypt Unlock an encrypted disk with a specified key
func OpenDiskCrypt(dc DiskCrypt) error {
	var err error
	switch dc.KeyType {
	case "string":
		{
			err = cryptctl.LuksOpenVolume(dc.Path, dc.TargetName, []byte(dc.Key))
			if err != nil {
				return err
			}
		}
	case "path":
		{
			keyfile, err := ioutil.ReadFile(dc.Key)
			if err != nil {
				return err
			}

			err = cryptctl.LuksOpenVolume(dc.Path, dc.TargetName, keyfile)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

// OpenDiskCrypts Unlock a set of encrypted disks
func OpenDiskCrypts(dcs []DiskCrypt) error {
	for _, dc := range dcs {
		err := OpenDiskCrypt(dc)
		if err != nil {
			return err
		}
	}

	return nil
}

// CloseDiskCrypt Lock an encrypted disk
func CloseDiskCrypt(dc DiskCrypt) error {
	err := cryptctl.LuksCloseVolume(dc.TargetName)
	if err != nil {
		return err
	}

	return nil
}

// CloseDiskCrypts Lock a set of encrypted disks
func CloseDiskCrypts(dcs []DiskCrypt) error {
	for _, dc := range dcs {
		err := CloseDiskCrypt(dc)
		if err != nil {
			return err
		}
	}

	return nil
}
