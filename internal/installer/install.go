// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package installer

import (
	"log"
)

// InstallInfo Describes the overall installation configuration
type InstallInfo struct {
	BlockDevice     string           `yaml:"block_device"`
	Upgrade         bool             `yaml:"upgrade"`
	ProductName     string           `yaml:"product_name"`
	Username        string           `yaml:"username"`
	Password        string           `yaml:"password"`
	Certificate     string           `yaml:"certificate"`
	PartitionTables []PartitionTable `yaml:"partition_tables,flow"`
	Partitions      []Partition      `yaml:"partitions,flow"`
	PhysicalVolumes []PhysicalVolume `yaml:"physical_volumes,flow"`
	DiskCrypts      []DiskCrypt      `yaml:"disk_crypts,flow"`
	FileSystems     []FileSystem     `yaml:"file_systems,flow"`
	InstallFiles    []InstallFile    `yaml:"install_files,flow"`
	EFIConfig       EFIConfiguration `yaml:"efi_config,flow"`
}

// PerformInstall Creates an installation according to the information in InstallInfo
func PerformInstall(i InstallInfo) error {
	err := CreatePartitionTables(i.BlockDevice, i.PartitionTables)
	if err != nil {
		return err
	}

	err = CreatePartitions(i.BlockDevice, i.Partitions)
	if err != nil {
		return err
	}

	err = CreatePhysicalVolumes(i.PhysicalVolumes)
	if err != nil {
		return err
	}

	err = CreateVolumeGroups(i.PhysicalVolumes)
	if err != nil {
		return err
	}

	err = CreateLogicalVolumes(i.PhysicalVolumes)
	if err != nil {
		return err
	}

	err = CreateDiskCrypts(i.DiskCrypts)
	if err != nil {
		return err
	}

	err = OpenDiskCrypts(i.DiskCrypts)
	if err != nil {
		return err
	}
	defer func() {
		err := CloseDiskCrypts(i.DiskCrypts)
		if err != nil {
			log.Printf("%v", err)
		}
	}()

	err = CreateFileSystems(i.FileSystems)
	if err != nil {
		return err
	}

	err = ConfigureEFI(i)
	if err != nil {
		return err
	}

	err = PlaceFiles(i)
	if err != nil {
		return err
	}

	return nil
}
