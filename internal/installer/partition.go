// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package installer

import (
	"fmt"
	"os/exec"
	"strings"

	"github.com/pkg/errors"
)

// PartitionTable Describes a partition table
type PartitionTable struct {
	Type string `yaml:"type"`
}

func (p PartitionTable) String() string {
	return fmt.Sprintf("Type: %v", p.Type)
}

// Partition Contains information required to create a partition
type Partition struct {
	Name       string `yaml:"name"`
	Filesystem string `yaml:"filesystem"`
	Start      string `yaml:"start"`
	End        string `yaml:"end"`
	ExtraArgs  string `yaml:"extra_args"`
}

func (p Partition) String() string {
	return fmt.Sprintf("Name: %v\nFilesystem: %v\nStart: %v\nEnd: %v", p.Name, p.Filesystem, p.Start, p.End)
}

func createPartitionTable(blockDevice string, t PartitionTable) error {
	gpt, err := exec.Command("parted", "--script", blockDevice, "mklabel", t.Type).CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to create partition table: \n%s", gpt)
	}

	return nil
}

// CreatePartitionTables Create a set of partition tables on a block device
func CreatePartitionTables(blockDevice string, pts []PartitionTable) error {
	for _, partitionTable := range pts {
		err := createPartitionTable(blockDevice, partitionTable)
		if err != nil {
			return err
		}
	}

	return nil
}

func createPartition(blockDevice string, p Partition) error {
	partitionCreate, err := exec.Command("parted", "--script", blockDevice, "mkpart", p.Name, p.Filesystem, p.Start, p.End).CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to create partition: \n%s", partitionCreate)
	}

	if p.ExtraArgs != "" {
		cmdString := []string{"--script", blockDevice}
		cmdString = append(cmdString, strings.Fields(p.ExtraArgs)...)

		applyFlags, err := exec.Command("parted", cmdString...).CombinedOutput()
		if err != nil {
			return errors.Wrapf(err, "failed to create partition: \n%s", applyFlags)
		}

	}

	return nil
}

// CreatePartitions Create a set of partitions on a block device
func CreatePartitions(blockDevice string, ps []Partition) error {
	for _, partition := range ps {
		err := createPartition(blockDevice, partition)
		if err != nil {
			return err
		}
	}

	return nil
}
